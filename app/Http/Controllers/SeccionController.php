<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Seccion;

class SeccionController extends Controller
{
  public function show($id)
    {

    }

  public function edit($id)
  {
      $seccion = Seccion::where('id', $id)
                      ->first();

      return view('edit', compact('seccion', 'id'));
  }

  public function update(Request $request, $id)
    {
        $seccion = new Seccion();
        $data = $this->validate($request, [
            'description'=>'required',
            'title'=> 'required'
        ]);
        $data['id'] = $id;
        $seccion->updateSeccion($data);

        return redirect('/home')->with('success', 'New Seccion has been updated!!');
    }
}
