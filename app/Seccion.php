<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Seccion extends Model
{
  protected $fillable = [ 'title', 'description', 'img_folder'];

  public function updateSeccion($data)
{
        $seccion = $this->find($data['id']);
        $seccion->title = $data['title'];
        $seccion->description = $data['description'];
        //$seccion->img_folder = $data['img_folder'];
        $seccion->save();
        return 1;
}
}
